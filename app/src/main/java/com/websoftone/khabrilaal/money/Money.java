package com.websoftone.khabrilaal.money;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.BollywoodArrayList;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.MoneyArrayList;
import com.websoftone.khabrilaal.money.adapter.MoneyAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Money extends Fragment implements ApiCallback.CategoryManagerCallback, MoneyAdapter.GridItemclick {
    @BindView(R.id.grid_money)
    RecyclerView gridMoney;
    private TabManager tabManager;
    private ArrayList<MoneyArrayList> moneyArrayLists;
    private ArrayList<MoneyArrayList> NewmoneyArrayLists;
    private Context context;
private String date;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_money, container, false);
        ButterKnife.bind(this, rootview);
        tabManager = new TabManager(this);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("Money").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllMoneyFromTable();
            }else {
//                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show();
                initiaziation();

            }
        }
        return rootview;
    }

    private void initiaziation() {
        moneyArrayLists = new ArrayList<>();
        NewmoneyArrayLists = new ArrayList<>();
        NewmoneyArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllMoneyFromLocalDb();
        if (null != NewmoneyArrayLists && NewmoneyArrayLists.size() > 0) {
            NewmoneyArrayLists = new ArrayList<>();
            NewmoneyArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllMoneyFromLocalDb();
            moneyArrayLists.addAll(NewmoneyArrayLists);
            setDataAdapter();
        }
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        moneyArrayLists = new ArrayList<>();
////        moneyArrayLists.addAll(categoryResponse.getData().getMoney());
////        setDataAdapter();

        SharedPreference.getInstance(context).setString("Money",date);

        for (int i = 0; i < categoryResponse.getData().getMoney().size(); i++) {
            MoneyArrayList contactNumber = new MoneyArrayList();
            contactNumber.setId(categoryResponse.getData().getMoney().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getMoney().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getMoney().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getMoney().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getMoney().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertMoney(contactNumber);
        }
        moneyArrayLists = new ArrayList<>();
        NewmoneyArrayLists = new ArrayList<>();
        NewmoneyArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllMoneyFromLocalDb();
        if (NewmoneyArrayLists.size() != 0) {
            moneyArrayLists.addAll(NewmoneyArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        MoneyAdapter newaAdapter = new MoneyAdapter(context, moneyArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridMoney) {
            gridMoney.setLayoutManager(layoutManager);
            gridMoney.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", moneyArrayLists.get(position).getUrl());
        startActivity(i);
    }
}

