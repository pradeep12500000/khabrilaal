package com.websoftone.khabrilaal.food;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.food.adapter.FoodAdapter;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.DanceArrayList;
import com.websoftone.khabrilaal.model.FoodArrayList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Food extends Fragment implements ApiCallback.CategoryManagerCallback, FoodAdapter.GridItemclick {
    @BindView(R.id.grid_food)
    RecyclerView gridFood;
    private TabManager tabManager;
    private ArrayList<FoodArrayList> foodArrayLists;
    private ArrayList<FoodArrayList> NewfoodArrayLists;
    private Context context;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_food, container, false);
        ButterKnife.bind(this, rootview);
        tabManager = new TabManager(this);


        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("Food").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllFoodFromTable();
            } else {
//                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show();
                initiaziation();

            }
        }
        return rootview;
    }

    private void initiaziation() {
        foodArrayLists = new ArrayList<>();
        NewfoodArrayLists = new ArrayList<>();
        NewfoodArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllFoodFromLocalDb();
        if (null != NewfoodArrayLists && NewfoodArrayLists.size() > 0) {
            NewfoodArrayLists = new ArrayList<>();
            NewfoodArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllFoodFromLocalDb();
            foodArrayLists.addAll(NewfoodArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        foodArrayLists = new ArrayList<>();
//        foodArrayLists.addAll(categoryResponse.getData().getFood());
//        setDataAdapter();
        SharedPreference.getInstance(context).setString("Food", date);

        for (int i = 0; i < categoryResponse.getData().getFood().size(); i++) {
            FoodArrayList contactNumber = new FoodArrayList();
            contactNumber.setId(categoryResponse.getData().getFood().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getFood().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getFood().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getFood().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getFood().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertFood(contactNumber);
        }
        foodArrayLists = new ArrayList<>();
        NewfoodArrayLists = new ArrayList<>();
        NewfoodArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllFoodFromLocalDb();
        if (NewfoodArrayLists.size() != 0) {
            foodArrayLists.addAll(NewfoodArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        FoodAdapter newaAdapter = new FoodAdapter(context, foodArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridFood) {
            gridFood.setLayoutManager(layoutManager);
            gridFood.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", foodArrayLists.get(position).getUrl());
        startActivity(i);
    }
}
