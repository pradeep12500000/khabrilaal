package com.websoftone.khabrilaal.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.NewsArrayList;
import com.websoftone.khabrilaal.news.adapter.NewaAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class NewsPaper extends Fragment implements ApiCallback.CategoryManagerCallback, NewaAdapter.GridItemclick {
    @BindView(R.id.recyclerViewNewsPaper)
    RecyclerView recyclerViewNewsPaper;
    public Context context;
    @BindView(R.id.eeee)
    TextView eeee;
    private View view;
    private TabManager tabManager;
    private ArrayList<NewsArrayList> newsMusicArrayLists;
    private ArrayList<NewsArrayList> NewnewsMusicArrayLists;
    private String date;
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news_paper, container, false);
        ButterKnife.bind(this, view);
        tabManager = new TabManager(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        firebaseAnalytics.setCurrentScreen(getActivity(), NewsPaper.class.getSimpleName(), NewsPaper.class.getSimpleName());


        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        eeee.setText(date);

        if (SharedPreference.getInstance(context).getString("NewsDate").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllDataFromTable();
            } else {
//                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show();
                initiaziation();

            }
        }

        return view;
    }

    private void initiaziation() {
        newsMusicArrayLists = new ArrayList<>();
        NewnewsMusicArrayLists = new ArrayList<>();
        NewnewsMusicArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllContactFromLocalDb();
        if (null != NewnewsMusicArrayLists && NewnewsMusicArrayLists.size() > 0) {
            NewnewsMusicArrayLists = new ArrayList<>();
            NewnewsMusicArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllContactFromLocalDb();
            newsMusicArrayLists.addAll(NewnewsMusicArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
        SharedPreference.getInstance(context).setString("NewsDate", date);

        for (int i = 0; i < categoryResponse.getData().getNews().size(); i++) {
            NewsArrayList contactNumber = new NewsArrayList();
            contactNumber.setId(categoryResponse.getData().getNews().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getNews().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getNews().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getNews().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getNews().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertNews(contactNumber);
        }
        newsMusicArrayLists = new ArrayList<>();
        NewnewsMusicArrayLists = new ArrayList<>();
        NewnewsMusicArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllContactFromLocalDb();
        if (NewnewsMusicArrayLists.size() != 0) {
            newsMusicArrayLists.addAll(NewnewsMusicArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        NewaAdapter newaAdapter = new NewaAdapter(context, newsMusicArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != recyclerViewNewsPaper) {
            recyclerViewNewsPaper.setLayoutManager(layoutManager);
            recyclerViewNewsPaper.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", newsMusicArrayLists.get(position).getUrl());
        startActivity(i);

    }
}
