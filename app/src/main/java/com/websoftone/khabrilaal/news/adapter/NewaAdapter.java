package com.websoftone.khabrilaal.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.model.NewsArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewaAdapter extends RecyclerView.Adapter<NewaAdapter.ViewHolder> {
    private Context context;
    private ArrayList<NewsArrayList> newsMusicArrayLists;
    private GridItemclick gridItemclick;

    public NewaAdapter(Context context, ArrayList<NewsArrayList> newsMusicArrayLists, GridItemclick gridItemclick) {
        this.context = context;
        this.newsMusicArrayLists = newsMusicArrayLists;
        this.gridItemclick = gridItemclick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_design, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load(newsMusicArrayLists.get(position).getImage()).placeholder(R.drawable.progress_animation).into(holder.gridImage);
        holder.gridText.setText(newsMusicArrayLists.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return newsMusicArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.grid_image)
        ImageView gridImage;
        @BindView(R.id.grid_text)
        TextView gridText;
        @BindView(R.id.CardClick)
        CardView CardClick;

        @OnClick(R.id.CardClick)
        public void onViewClicked() {
            gridItemclick.OnitemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface GridItemclick {
        void OnitemClick(int position);
    }
}
