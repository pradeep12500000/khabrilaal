package com.websoftone.khabrilaal.ui.main;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.websoftone.khabrilaal.bollywood.Bollywood;
import com.websoftone.khabrilaal.dance.Dance;
import com.websoftone.khabrilaal.food.Food;
import com.websoftone.khabrilaal.money.Money;
import com.websoftone.khabrilaal.motivaional.Motivational;
import com.websoftone.khabrilaal.comdey.Comedy;
import com.websoftone.khabrilaal.news.NewsPaper;
import com.websoftone.khabrilaal.tech.Tech;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    int tabCount;

    //Constructor to the class

    public SectionsPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    //Overriding method getItem

    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                NewsPaper Newspaper=new NewsPaper();
                return Newspaper;
            case 1:
                Motivational motivational=new Motivational();
                return motivational;
            case 2:
                Comedy comedy=new Comedy();
                return comedy;
            case 3:
                Dance dance =new Dance();
                return dance;
            case 4:
                Food food =new Food();
                return food;

            case 5:
                Tech tech =new Tech();
                return tech;
            case 6:
                Bollywood bollywood =new Bollywood();
                return bollywood;

            case 7:
                Money money =new Money();
                return money;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 8;
    }
    @Override
    public CharSequence getPageTitle(int postion) {
        switch (postion) {

            case 0:
                return "News";
            case 1:
                return "Motivational";
            case 2:
                return "Comedy";
            case 3:
                return "Dance";
            case 4:
                return "Food";
            case 5:
                return "Tech";
            case 6:
                return "Bollywood";
            case 7:
                return "Money";

            default:
                return null;

        }
    }
}