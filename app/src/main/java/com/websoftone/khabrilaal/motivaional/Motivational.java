package com.websoftone.khabrilaal.motivaional;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.MotovaionalArrayList;
import com.websoftone.khabrilaal.model.NewsArrayList;
import com.websoftone.khabrilaal.motivaional.adapter.MotivationalAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Motivational extends Fragment implements ApiCallback.CategoryManagerCallback, MotivationalAdapter.GridItemclick {
    @BindView(R.id.grid_motivational)
    RecyclerView gridMotivational;
    private Context context;
    private View view;
    private TabManager tabManager;
    private ArrayList<MotovaionalArrayList> motovaionalArrayLists;
    private ArrayList<MotovaionalArrayList> NewmotovaionalArrayLists;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_motivational, container, false);
        ButterKnife.bind(this, view);
        tabManager = new TabManager(this);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("Motivation").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllMotivationFromTable();
            } else {
//                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show();
                initiaziation();

            }
        }
        return view;
    }


    private void initiaziation() {
        motovaionalArrayLists = new ArrayList<>();
        NewmotovaionalArrayLists = new ArrayList<>();
        NewmotovaionalArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllMotivationFromLocalDb();
        if (null != NewmotovaionalArrayLists && NewmotovaionalArrayLists.size() > 0) {
            NewmotovaionalArrayLists = new ArrayList<>();
            NewmotovaionalArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllMotivationFromLocalDb();
            motovaionalArrayLists.addAll(NewmotovaionalArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        motovaionalArrayLists = new ArrayList<>();
//        motovaionalArrayLists.addAll(categoryResponse.getData().getMotovaional());
//        setDataAdapter();
        SharedPreference.getInstance(context).setString("Motivation", date);

        for (int i = 0; i < categoryResponse.getData().getMotovaional().size(); i++) {
            MotovaionalArrayList contactNumber = new MotovaionalArrayList();
            contactNumber.setId(categoryResponse.getData().getMotovaional().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getMotovaional().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getMotovaional().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getMotovaional().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getMotovaional().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertMotivation(contactNumber);
        }
        motovaionalArrayLists = new ArrayList<>();
        NewmotovaionalArrayLists = new ArrayList<>();
        NewmotovaionalArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllMotivationFromLocalDb();
        if (NewmotovaionalArrayLists.size() != 0) {
            motovaionalArrayLists.addAll(NewmotovaionalArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        MotivationalAdapter newaAdapter = new MotivationalAdapter(context, motovaionalArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridMotivational) {
            gridMotivational.setLayoutManager(layoutManager);
            gridMotivational.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", motovaionalArrayLists.get(position).getUrl());
        startActivity(i);
    }
}
