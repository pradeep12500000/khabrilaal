package com.websoftone.khabrilaal.motivaional.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.model.MotovaionalArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MotivationalAdapter extends RecyclerView.Adapter<MotivationalAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MotovaionalArrayList> motovaionalArrayLists;
    private GridItemclick gridItemclick;

    public MotivationalAdapter(Context context, ArrayList<MotovaionalArrayList> motovaionalArrayLists, GridItemclick gridItemclick) {
        this.context = context;
        this.motovaionalArrayLists = motovaionalArrayLists;
        this.gridItemclick = gridItemclick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_design, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load(motovaionalArrayLists.get(position).getImage()).placeholder(R.drawable.progress_animation).into(holder.gridImage);
        holder.gridText.setText(motovaionalArrayLists.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return motovaionalArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.grid_image)
        ImageView gridImage;
        @BindView(R.id.grid_text)
        TextView gridText;
        @BindView(R.id.CardClick)
        CardView CardClick;

        @OnClick(R.id.CardClick)
        public void onViewClicked() {
            gridItemclick.OnitemClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface GridItemclick {
        void OnitemClick(int position);
    }
}
