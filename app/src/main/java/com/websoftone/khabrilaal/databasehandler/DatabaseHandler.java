package com.websoftone.khabrilaal.databasehandler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.websoftone.khabrilaal.model.BollywoodArrayList;
import com.websoftone.khabrilaal.model.ComedyArrayList;
import com.websoftone.khabrilaal.model.DanceArrayList;
import com.websoftone.khabrilaal.model.FoodArrayList;
import com.websoftone.khabrilaal.model.MoneyArrayList;
import com.websoftone.khabrilaal.model.MotovaionalArrayList;
import com.websoftone.khabrilaal.model.NewsArrayList;
import com.websoftone.khabrilaal.model.TechArrayList;

import java.util.ArrayList;


public class DatabaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "WalitHowzIt.db";
    private static final int DATABASE_VERSION = 1;
    private ContactInteraction contactInteraction;


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.contactInteraction = new ContactInteraction(this);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_CONTACT);
        db.execSQL(Constant.CREATE_TABLE_CONTACT_Motivation);
        db.execSQL(Constant.CREATE_TABLE_Comedy);
        db.execSQL(Constant.CREATE_TABLE_Dance);
        db.execSQL(Constant.CREATE_TABLE_Food);
        db.execSQL(Constant.CREATE_TABLE_Tech);
        db.execSQL(Constant.CREATE_TABLE_Bollywood);
        db.execSQL(Constant.CREATE_TABLE_Money);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newViersion) {
        db.execSQL(Constant.DROP_TABLE_CONTACT);
        db.execSQL(Constant.DROP_TABLE_CONTACT_MOTIVATION);
        db.execSQL(Constant.DROP_TABLE_CONTACT_Comedy);
        db.execSQL(Constant.DROP_TABLE_CONTACT_Dance);
        db.execSQL(Constant.DROP_TABLE_CONTACT_Food);
        db.execSQL(Constant.DROP_TABLE_CONTACT_Tech);
        db.execSQL(Constant.DROP_TABLE_CONTACT_Bollywood);
        db.execSQL(Constant.DROP_TABLE_CONTACT_Money);
    }


    /*---------------------------------------- function for contact -----------------------------------------------------------------*/

    public void deleteAllDataFromTable(){
        contactInteraction.deleteAllDataFromTable();
    }

    public void deleteAllMotivationFromTable(){
        contactInteraction.deleteAllMotivationFromTable();
    }

    public void deleteAllComedyFromTable(){
        contactInteraction.deleteAllComedyFromTable();
    }

    public void deleteAllDanceFromTable(){
        contactInteraction.deleteAllDanceFromTable();
    }

    public void deleteAllFoodFromTable(){
        contactInteraction.deleteAllFoodFromTable();
    }

    public void deleteAllTechFromTable(){
        contactInteraction.deleteAllTechFromTable();
    }

    public void deleteAllBollywoodFromTable(){
        contactInteraction.deleteAllBollywoodFromTable();
    }

    public void deleteAllMoneyFromTable(){
        contactInteraction.deleteAllMoneyFromTable();
    }

    public boolean insertNews(NewsArrayList datum) {
        return contactInteraction.insertNews(datum);
    }

    public ArrayList<NewsArrayList> getAllContactFromLocalDb() {
        return contactInteraction.getAllContactFromLocalDb();
    }

    public boolean insertMotivation(MotovaionalArrayList datum) {
        return contactInteraction.insertMotivation(datum);
    }

    public ArrayList<MotovaionalArrayList> getAllMotivationFromLocalDb() {
        return contactInteraction.getAllMotivationFromLocalDb();
    }


    public boolean insertComedy(ComedyArrayList datum) {
        return contactInteraction.insertComdey(datum);
    }

    public ArrayList<ComedyArrayList> getAllComedyFromLocalDb() {
        return contactInteraction.getAllComedyFromLocalDb();
    }


    public boolean insertDance(DanceArrayList datum) {
        return contactInteraction.insertDance(datum);
    }

    public ArrayList<DanceArrayList> getAllDanceFromLocalDb() {
        return contactInteraction.getAllDanceFromLocalDb();
    }


    public boolean insertFood(FoodArrayList datum) {
        return contactInteraction.insertFood(datum);
    }

    public ArrayList<FoodArrayList> getAllFoodFromLocalDb() {
        return contactInteraction.getAllFoodFromLocalDb();
    }


    public boolean insertTech(TechArrayList datum) {
        return contactInteraction.insertTech(datum);
    }

    public ArrayList<TechArrayList> getAllTechFromLocalDb() {
        return contactInteraction.getAllTechFromLocalDb();
    }


    public boolean insertBollywood(BollywoodArrayList datum) {
        return contactInteraction.insertBollywood(datum);
    }

    public ArrayList<BollywoodArrayList> getAllBollywoodFromLocalDb() {
        return contactInteraction.getAllBollywoodFromLocalDb();
    }


    public boolean insertMoney(MoneyArrayList datum) {
        return contactInteraction.insertMoney(datum);
    }

    public ArrayList<MoneyArrayList> getAllMoneyFromLocalDb() {
        return contactInteraction.getAllMoneyFromLocalDb();
    }
}
