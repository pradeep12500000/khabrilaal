package com.websoftone.khabrilaal.databasehandler;

import android.content.Context;



public class DBHelperSingleton {
    public static DatabaseHandler mDBDbHelper = null;
    private static final DBHelperSingleton ourInstance = new DBHelperSingleton();

    public static DBHelperSingleton getInstance() {
        return ourInstance;
    }

    private DBHelperSingleton() {
    }

    public void initDB(Context context){
        mDBDbHelper = new DatabaseHandler(context);
    }
}
