package com.websoftone.khabrilaal.databasehandler;

import android.app.Application;

public class KhabrilalApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DBHelperSingleton.getInstance().initDB(this);
    }
}
