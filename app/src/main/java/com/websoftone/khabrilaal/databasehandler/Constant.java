package com.websoftone.khabrilaal.databasehandler;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;


public class Constant {

    /*---------------------constant for database-----------------------------------*/
    /*constants for databse*/
    public static final String CONTACT_TABLE="contactTable";

    public static final String ID = "id";
    public static final String SELF_ID = "self_id";
    public static final String OTHER_USER_ID = "other_user_id";
    public static final String OTHER_USER_NAME = "other_user_name";
    public static final String AVATAR = "avatar";
    public static final String CONTACT_NUMBER = "contact";


    public static final String CREATE_TABLE_CONTACT="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE + "("
            + ID + " TEXT,"
            + SELF_ID + " TEXT,"
            + OTHER_USER_ID + " TEXT,"
            + OTHER_USER_NAME + " TEXT,"
            + AVATAR + " TEXT,"
            + CONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT="DROP TABLE IF EXISTS"+CONTACT_TABLE;

    public static final String SELECT_TABLE_CONTACT="SELECT * FROM "+ CONTACT_TABLE +" ORDER BY "+OTHER_USER_NAME+" ASC";


    /*constants for databse*/
    public static final String CONTACT_TABLE_MOTIVATION="MotivationTable";

    public static final String MotivationID = "Motivationid";
    public static final String MotivationSELF_ID = "Motivationself_id";
    public static final String MotivationOTHER_USER_ID = "Motivationother_user_id";
    public static final String MotivationOTHER_USER_NAME = "Motivationother_user_name";
    public static final String MotivationAVATAR = "Motivationavatar";
    public static final String MotivationCONTACT_NUMBER = "Motivationcontact";


    public static final String CREATE_TABLE_CONTACT_Motivation="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_MOTIVATION + "("
            + MotivationID + " TEXT,"
            + MotivationSELF_ID + " TEXT,"
            + MotivationOTHER_USER_ID + " TEXT,"
            + MotivationOTHER_USER_NAME + " TEXT,"
            + MotivationAVATAR + " TEXT,"
            + MotivationCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_MOTIVATION="DROP TABLE IF EXISTS"+CONTACT_TABLE_MOTIVATION;

    public static final String SELECT_TABLE_CONTACT_MOTIVATION="SELECT * FROM "+ CONTACT_TABLE_MOTIVATION +" ORDER BY "+MotivationOTHER_USER_NAME+" ASC";




    /*constants for databse*/
    public static final String CONTACT_TABLE_COMEDY="ComedyTable";

    public static final String ComedyID = "Comedyid";
    public static final String ComedySELF_ID = "Comedyself_id";
    public static final String ComedyOTHER_USER_ID = "Comedyother_user_id";
    public static final String ComedyOTHER_USER_NAME = "Comedyother_user_name";
    public static final String ComedyAVATAR = "Comedyavatar";
    public static final String ComedyCONTACT_NUMBER = "Comedycontact";


    public static final String CREATE_TABLE_Comedy="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_COMEDY + "("
            + ComedyID + " TEXT,"
            + ComedySELF_ID + " TEXT,"
            + ComedyOTHER_USER_ID + " TEXT,"
            + ComedyOTHER_USER_NAME + " TEXT,"
            + ComedyAVATAR + " TEXT,"
            + ComedyCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_Comedy="DROP TABLE IF EXISTS"+CONTACT_TABLE_COMEDY;

    public static final String SELECT_TABLE_CONTACT_Comedy="SELECT * FROM "+ CONTACT_TABLE_COMEDY +" ORDER BY "+ComedyOTHER_USER_NAME+" ASC";






    /*constants for databse*/
    public static final String CONTACT_TABLE_Dance="DanceTable";

    public static final String DanceID = "Danceid";
    public static final String DanceSELF_ID = "Danceself_id";
    public static final String DanceOTHER_USER_ID = "Danceother_user_id";
    public static final String DanceOTHER_USER_NAME = "Danceother_user_name";
    public static final String DanceAVATAR = "Danceavatar";
    public static final String DanceCONTACT_NUMBER = "Dancecontact";


    public static final String CREATE_TABLE_Dance="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_Dance + "("
            + DanceID + " TEXT,"
            + DanceSELF_ID + " TEXT,"
            + DanceOTHER_USER_ID + " TEXT,"
            + DanceOTHER_USER_NAME + " TEXT,"
            + DanceAVATAR + " TEXT,"
            + DanceCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_Dance="DROP TABLE IF EXISTS"+CONTACT_TABLE_Dance;

    public static final String SELECT_TABLE_CONTACT_Dance="SELECT * FROM "+ CONTACT_TABLE_Dance +" ORDER BY "+DanceOTHER_USER_NAME+" ASC";



    /*constants for databse*/
    public static final String CONTACT_TABLE_Food="FoodTable";

    public static final String FoodID = "Foodid";
    public static final String FoodSELF_ID = "Foodself_id";
    public static final String FoodOTHER_USER_ID = "Foodother_user_id";
    public static final String FoodOTHER_USER_NAME = "Foodother_user_name";
    public static final String FoodAVATAR = "Foodavatar";
    public static final String FoodCONTACT_NUMBER = "Foodcontact";


    public static final String CREATE_TABLE_Food="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_Food + "("
            + FoodID + " TEXT,"
            + FoodSELF_ID + " TEXT,"
            + FoodOTHER_USER_ID + " TEXT,"
            + FoodOTHER_USER_NAME + " TEXT,"
            + FoodAVATAR + " TEXT,"
            + FoodCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_Food="DROP TABLE IF EXISTS"+CONTACT_TABLE_Food;

    public static final String SELECT_TABLE_CONTACT_Food="SELECT * FROM "+ CONTACT_TABLE_Food +" ORDER BY "+FoodOTHER_USER_NAME+" ASC";



    /*constants for databse*/
    public static final String CONTACT_TABLE_Tech="TechTable";

    public static final String TechID = "Techid";
    public static final String TechSELF_ID = "Techself_id";
    public static final String TechOTHER_USER_ID = "Techother_user_id";
    public static final String TechOTHER_USER_NAME = "Techother_user_name";
    public static final String TechAVATAR = "Techavatar";
    public static final String TechCONTACT_NUMBER = "Techcontact";


    public static final String CREATE_TABLE_Tech="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_Tech + "("
            + TechID + " TEXT,"
            + TechSELF_ID + " TEXT,"
            + TechOTHER_USER_ID + " TEXT,"
            + TechOTHER_USER_NAME + " TEXT,"
            + TechAVATAR + " TEXT,"
            + TechCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_Tech="DROP TABLE IF EXISTS"+CONTACT_TABLE_Tech;

    public static final String SELECT_TABLE_CONTACT_Tech="SELECT * FROM "+ CONTACT_TABLE_Tech +" ORDER BY "+TechOTHER_USER_NAME+" ASC";


       /*constants for databse*/
    public static final String CONTACT_TABLE_Bollywood="BollywoodTable";

    public static final String BollywoodID = "Bollywoodid";
    public static final String BollywoodSELF_ID = "Bollywoodself_id";
    public static final String BollywoodOTHER_USER_ID = "Bollywoodother_user_id";
    public static final String BollywoodOTHER_USER_NAME = "Bollywoodother_user_name";
    public static final String BollywoodAVATAR = "Bollywoodavatar";
    public static final String BollywoodCONTACT_NUMBER = "Bollywoodcontact";
    public static final int PERMISSION_ALL = 1;


    public static final String CREATE_TABLE_Bollywood="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_Bollywood + "("
            + BollywoodID + " TEXT,"
            + BollywoodSELF_ID + " TEXT,"
            + BollywoodOTHER_USER_ID + " TEXT,"
            + BollywoodOTHER_USER_NAME + " TEXT,"
            + BollywoodAVATAR + " TEXT,"
            + BollywoodCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_Bollywood="DROP TABLE IF EXISTS"+CONTACT_TABLE_Bollywood;

    public static final String SELECT_TABLE_CONTACT_Bollywood="SELECT * FROM "+ CONTACT_TABLE_Bollywood +" ORDER BY "+BollywoodOTHER_USER_NAME+" ASC";



    /*constants for databse*/
    public static final String CONTACT_TABLE_Money="MoneyTable";

    public static final String MoneyID = "Moneyid";
    public static final String MoneySELF_ID = "Moneyself_id";
    public static final String MoneyOTHER_USER_ID = "Moneyother_user_id";
    public static final String MoneyOTHER_USER_NAME = "Moneyother_user_name";
    public static final String MoneyAVATAR = "Moneyavatar";
    public static final String MoneyCONTACT_NUMBER = "Moneycontact";


    public static final String CREATE_TABLE_Money="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE_Money + "("
            + MoneyID + " TEXT,"
            + MoneySELF_ID + " TEXT,"
            + MoneyOTHER_USER_ID + " TEXT,"
            + MoneyOTHER_USER_NAME + " TEXT,"
            + MoneyAVATAR + " TEXT,"
            + MoneyCONTACT_NUMBER + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT_Money="DROP TABLE IF EXISTS"+CONTACT_TABLE_Money;

    public static final String SELECT_TABLE_CONTACT_Money="SELECT * FROM "+ CONTACT_TABLE_Money +" ORDER BY "+MoneyOTHER_USER_NAME+" ASC";

    /*--------------------------------permission check method -------------------------------*/
    public static void hasPermissions(Context context, String[] strings, Activity activity) {
        if (context != null && strings != null) {
            for (String permission : strings) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, strings, PERMISSION_ALL);
                    break;
                }
            }
        }
    }

}
