package com.websoftone.khabrilaal.databasehandler;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.websoftone.khabrilaal.model.BollywoodArrayList;
import com.websoftone.khabrilaal.model.ComedyArrayList;
import com.websoftone.khabrilaal.model.DanceArrayList;
import com.websoftone.khabrilaal.model.FoodArrayList;
import com.websoftone.khabrilaal.model.MoneyArrayList;
import com.websoftone.khabrilaal.model.MotovaionalArrayList;
import com.websoftone.khabrilaal.model.NewsArrayList;
import com.websoftone.khabrilaal.model.TechArrayList;

import java.util.ArrayList;



public class ContactInteraction {

    private SQLiteDatabase dbReadable, dbWritable;

    public ContactInteraction(DatabaseHandler databaseHandler) {
        this.dbReadable = databaseHandler.getReadableDatabase();
        this.dbWritable = databaseHandler.getWritableDatabase();
    }

    public void deleteAllDataFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE);
    }

    public void deleteAllMotivationFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_MOTIVATION);
    }

    public void deleteAllComedyFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_COMEDY);
    }

    public void deleteAllDanceFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_Dance);
    }

    public void deleteAllFoodFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_Food);
    }

    public void deleteAllTechFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_Tech);
    }

    public void deleteAllBollywoodFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_Bollywood);
    }

    public void deleteAllMoneyFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE_Money);
    }


    public boolean insertNews(NewsArrayList datum) {
            long updateId = dbWritable.insert(Constant.CONTACT_TABLE, null, this.getObject(datum));
        return updateId != -1;

    }

    private ContentValues getObject(NewsArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.ID, datum.getId());
        contentValues.put(Constant.SELF_ID, datum.getCategory());
        contentValues.put(Constant.OTHER_USER_ID, datum.getName());
        contentValues.put(Constant.OTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.AVATAR, datum.getImage());
        contentValues.put(Constant.CONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<NewsArrayList> getAllContactFromLocalDb() {
        ArrayList<NewsArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    NewsArrayList datum = new NewsArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.ID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.SELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.OTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.OTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.AVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.CONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }



    public boolean insertMotivation(MotovaionalArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_MOTIVATION, null, this.getObjectMotivation(datum));
        return updateId != -1;

    }

    private ContentValues getObjectMotivation(MotovaionalArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.MotivationID, datum.getId());
        contentValues.put(Constant.MotivationSELF_ID, datum.getCategory());
        contentValues.put(Constant.MotivationOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.MotivationOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.MotivationAVATAR, datum.getImage());
        contentValues.put(Constant.MotivationCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<MotovaionalArrayList> getAllMotivationFromLocalDb() {
        ArrayList<MotovaionalArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_MOTIVATION, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    MotovaionalArrayList datum = new MotovaionalArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.MotivationID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.MotivationSELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.MotivationOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.MotivationOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.MotivationAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.MotivationCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }


    public boolean insertComdey(ComedyArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_COMEDY, null, this.getObjectComedy(datum));
        return updateId != -1;

    }

    private ContentValues getObjectComedy(ComedyArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.ComedyID, datum.getId());
        contentValues.put(Constant.ComedySELF_ID, datum.getCategory());
        contentValues.put(Constant.ComedyOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.ComedyOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.ComedyAVATAR, datum.getImage());
        contentValues.put(Constant.ComedyCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<ComedyArrayList> getAllComedyFromLocalDb() {
        ArrayList<ComedyArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_Comedy, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    ComedyArrayList datum = new ComedyArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.ComedyID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.ComedySELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.ComedyOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.ComedyOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.ComedyAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.ComedyCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }




    public boolean insertDance(DanceArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_Dance, null, this.getObjectDance(datum));
        return updateId != -1;

    }

    private ContentValues getObjectDance(DanceArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.DanceID, datum.getId());
        contentValues.put(Constant.DanceSELF_ID, datum.getCategory());
        contentValues.put(Constant.DanceOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.DanceOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.DanceAVATAR, datum.getImage());
        contentValues.put(Constant.DanceCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<DanceArrayList> getAllDanceFromLocalDb() {
        ArrayList<DanceArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_Dance, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    DanceArrayList datum = new DanceArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.DanceID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.DanceSELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.DanceOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.DanceOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.DanceAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.DanceCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }


    public boolean insertFood(FoodArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_Food, null, this.getObjectFood(datum));
        return updateId != -1;

    }

    private ContentValues getObjectFood(FoodArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.FoodID, datum.getId());
        contentValues.put(Constant.FoodSELF_ID, datum.getCategory());
        contentValues.put(Constant.FoodOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.FoodOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.FoodAVATAR, datum.getImage());
        contentValues.put(Constant.FoodCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<FoodArrayList> getAllFoodFromLocalDb() {
        ArrayList<FoodArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_Food, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    FoodArrayList datum = new FoodArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.FoodID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.FoodSELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.FoodOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.FoodOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.FoodAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.FoodCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }



    public boolean insertTech(TechArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_Tech, null, this.getObjectTech(datum));
        return updateId != -1;

    }

    private ContentValues getObjectTech(TechArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.TechID, datum.getId());
        contentValues.put(Constant.TechSELF_ID, datum.getCategory());
        contentValues.put(Constant.TechOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.TechOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.TechAVATAR, datum.getImage());
        contentValues.put(Constant.TechCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<TechArrayList> getAllTechFromLocalDb() {
        ArrayList<TechArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_Tech, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    TechArrayList datum = new TechArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.TechID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.TechSELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.TechOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.TechOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.TechAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.TechCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }



    public boolean insertBollywood(BollywoodArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_Bollywood, null, this.getObjectBollywood(datum));
        return updateId != -1;

    }

    private ContentValues getObjectBollywood(BollywoodArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.BollywoodID, datum.getId());
        contentValues.put(Constant.BollywoodSELF_ID, datum.getCategory());
        contentValues.put(Constant.BollywoodOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.BollywoodOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.BollywoodAVATAR, datum.getImage());
        contentValues.put(Constant.BollywoodCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<BollywoodArrayList> getAllBollywoodFromLocalDb() {
        ArrayList<BollywoodArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_Bollywood, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    BollywoodArrayList datum = new BollywoodArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.BollywoodID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.BollywoodSELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.BollywoodOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.BollywoodOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.BollywoodAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.BollywoodCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }





    public boolean insertMoney(MoneyArrayList datum) {
        long updateId = dbWritable.insert(Constant.CONTACT_TABLE_Money, null, this.getObjectMoney(datum));
        return updateId != -1;

    }

    private ContentValues getObjectMoney(MoneyArrayList datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.MoneyID, datum.getId());
        contentValues.put(Constant.MoneySELF_ID, datum.getCategory());
        contentValues.put(Constant.MoneyOTHER_USER_ID, datum.getName());
        contentValues.put(Constant.MoneyOTHER_USER_NAME, datum.getName());
        contentValues.put(Constant.MoneyAVATAR, datum.getImage());
        contentValues.put(Constant.MoneyCONTACT_NUMBER, datum.getUrl());
        return contentValues;
    }

    public ArrayList<MoneyArrayList> getAllMoneyFromLocalDb() {
        ArrayList<MoneyArrayList> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT_Money, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    MoneyArrayList datum = new MoneyArrayList();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.MoneyID)));
                    datum.setCategory(cursor.getString(cursor.getColumnIndex(Constant.MoneySELF_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.MoneyOTHER_USER_ID)));
                    datum.setName(cursor.getString(cursor.getColumnIndex(Constant.MoneyOTHER_USER_NAME)));
                    datum.setImage(cursor.getString(cursor.getColumnIndex(Constant.MoneyAVATAR)));
                    datum.setUrl(cursor.getString(cursor.getColumnIndex(Constant.MoneyCONTACT_NUMBER)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }
}
