package com.websoftone.khabrilaal.comdey;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.comdey.adapter.ComdeyAdapter;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.ComedyArrayList;
import com.websoftone.khabrilaal.model.MotovaionalArrayList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Comedy extends Fragment implements ApiCallback.CategoryManagerCallback, ComdeyAdapter.GridItemclick {
    @BindView(R.id.grid_comedy)
    RecyclerView gridComedy;
    private Context context;
    private View view;
    private TabManager tabManager;
    private ArrayList<ComedyArrayList> comedyArrayLists;
    private ArrayList<ComedyArrayList> NewcomedyArrayLists;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comedy, container, false);
        ButterKnife.bind(this, view);
        tabManager = new TabManager(this);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("Comedy").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllComedyFromTable();
            } else {
//                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show();
                initiaziation();

            }
        }
        return view;
    }

    private void initiaziation() {
        comedyArrayLists = new ArrayList<>();
        NewcomedyArrayLists = new ArrayList<>();
        NewcomedyArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllComedyFromLocalDb();
        if (null != NewcomedyArrayLists && NewcomedyArrayLists.size() > 0) {
            NewcomedyArrayLists = new ArrayList<>();
            NewcomedyArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllComedyFromLocalDb();
            comedyArrayLists.addAll(NewcomedyArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        comedyArrayLists = new ArrayList<>();
//        comedyArrayLists.addAll(categoryResponse.getData().getComedy());
//        setDataAdapter();
        SharedPreference.getInstance(context).setString("Comedy", date);

        for (int i = 0; i < categoryResponse.getData().getComedy().size(); i++) {
            ComedyArrayList contactNumber = new ComedyArrayList();
            contactNumber.setId(categoryResponse.getData().getComedy().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getComedy().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getComedy().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getComedy().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getComedy().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertComedy(contactNumber);
        }
        comedyArrayLists = new ArrayList<>();
        NewcomedyArrayLists = new ArrayList<>();
        NewcomedyArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllComedyFromLocalDb();
        if (NewcomedyArrayLists.size() != 0) {
            comedyArrayLists.addAll(NewcomedyArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        ComdeyAdapter newaAdapter = new ComdeyAdapter(context, comedyArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridComedy) {
            gridComedy.setLayoutManager(layoutManager);
            gridComedy.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", comedyArrayLists.get(position).getUrl());
        startActivity(i);
    }
}

