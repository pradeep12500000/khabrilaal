package com.websoftone.khabrilaal.dance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.dance.adapter.DanceAdapter;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.ComedyArrayList;
import com.websoftone.khabrilaal.model.DanceArrayList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Dance extends Fragment implements ApiCallback.CategoryManagerCallback, DanceAdapter.GridItemclick {
    @BindView(R.id.grid_dance)
    RecyclerView gridDance;
    private TabManager tabManager;
    private ArrayList<DanceArrayList> danceArrayLists;
    private ArrayList<DanceArrayList> NewdanceArrayLists;
    private Context context;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_dance, container, false);
        ButterKnife.bind(this, rootview);
        ButterKnife.bind(this, rootview);
        tabManager = new TabManager(this);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("dance").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllDanceFromTable();
            } else {
//                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show();
                initiaziation();

            }
        }
        return rootview;
    }

    private void initiaziation() {
        danceArrayLists = new ArrayList<>();
        NewdanceArrayLists = new ArrayList<>();
        NewdanceArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllDanceFromLocalDb();
        if (null != NewdanceArrayLists && NewdanceArrayLists.size() > 0) {
            NewdanceArrayLists = new ArrayList<>();
            NewdanceArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllDanceFromLocalDb();
            danceArrayLists.addAll(NewdanceArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        danceArrayLists = new ArrayList<>();
//        danceArrayLists.addAll(categoryResponse.getData().getDance());
//        setDataAdapter();

        SharedPreference.getInstance(context).setString("dance", date);

        for (int i = 0; i < categoryResponse.getData().getDance().size(); i++) {
            DanceArrayList contactNumber = new DanceArrayList();
            contactNumber.setId(categoryResponse.getData().getDance().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getDance().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getDance().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getDance().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getDance().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertDance(contactNumber);
        }
        danceArrayLists = new ArrayList<>();
        NewdanceArrayLists = new ArrayList<>();
        NewdanceArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllDanceFromLocalDb();
        if (NewdanceArrayLists.size() != 0) {
            danceArrayLists.addAll(NewdanceArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        DanceAdapter newaAdapter = new DanceAdapter(context, danceArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridDance) {
            gridDance.setLayoutManager(layoutManager);
            gridDance.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", danceArrayLists.get(position).getUrl());
        startActivity(i);
    }
}