package com.websoftone.khabrilaal.bollywood;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.LoadUrlActivity;
import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.bollywood.adapter.BollywoodAdapter;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.BollywoodArrayList;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.TechArrayList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Bollywood extends Fragment implements ApiCallback.CategoryManagerCallback, BollywoodAdapter.GridItemclick {
    @BindView(R.id.grid_bollywood)
    RecyclerView gridBollywood;
    private TabManager tabManager;
    private ArrayList<BollywoodArrayList> bollywoodArrayLists;
    private ArrayList<BollywoodArrayList> NewbollywoodArrayLists;
    private Context context;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_bollywood, container, false);
        ButterKnife.bind(this, rootview);
        tabManager = new TabManager(this);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("Bollywood").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllBollywoodFromTable();
            } else {
                initiaziation();
            }
        }
        return rootview;
    }

    private void initiaziation() {
        bollywoodArrayLists = new ArrayList<>();
        NewbollywoodArrayLists = new ArrayList<>();
        NewbollywoodArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllBollywoodFromLocalDb();
        if (null != NewbollywoodArrayLists && NewbollywoodArrayLists.size() > 0) {
            NewbollywoodArrayLists = new ArrayList<>();
            NewbollywoodArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllBollywoodFromLocalDb();
            bollywoodArrayLists.addAll(NewbollywoodArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        bollywoodArrayLists = new ArrayList<>();
//        bollywoodArrayLists.addAll(categoryResponse.getData().getBollywood());
//        setDataAdapter();

        SharedPreference.getInstance(context).setString("Bollywood", date);

        for (int i = 0; i < categoryResponse.getData().getBollywood().size(); i++) {
            BollywoodArrayList contactNumber = new BollywoodArrayList();
            contactNumber.setId(categoryResponse.getData().getBollywood().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getBollywood().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getBollywood().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getBollywood().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getBollywood().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertBollywood(contactNumber);
        }
        bollywoodArrayLists = new ArrayList<>();
        NewbollywoodArrayLists = new ArrayList<>();
        NewbollywoodArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllBollywoodFromLocalDb();
        if (NewbollywoodArrayLists.size() != 0) {
            bollywoodArrayLists.addAll(NewbollywoodArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        BollywoodAdapter newaAdapter = new BollywoodAdapter(context, bollywoodArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridBollywood) {
            gridBollywood.setLayoutManager(layoutManager);
            gridBollywood.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {
        Intent i = new Intent(getContext(), LoadUrlActivity.class);
        i.putExtra("url", bollywoodArrayLists.get(position).getUrl());
        startActivity(i);
    }
}
