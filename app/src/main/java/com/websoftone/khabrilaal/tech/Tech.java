package com.websoftone.khabrilaal.tech;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.websoftone.khabrilaal.R;
import com.websoftone.khabrilaal.TabManager;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.databasehandler.DBHelperSingleton;
import com.websoftone.khabrilaal.databasehandler.SharedPreference;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.model.FoodArrayList;
import com.websoftone.khabrilaal.model.TechArrayList;
import com.websoftone.khabrilaal.tech.adapter.TechAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.websoftone.khabrilaal.basic.BaseActivity.isInternetConneted;


public class Tech extends Fragment implements ApiCallback.CategoryManagerCallback, TechAdapter.GridItemclick {
    @BindView(R.id.grid_tech)
    RecyclerView gridTech;
    private Context context;
    private View view;
    private TabManager tabManager;
    private ArrayList<TechArrayList> techArrayLists;
    private ArrayList<TechArrayList> NewtechArrayLists;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_tech, container, false);
        ButterKnife.bind(this, rootview);
        tabManager = new TabManager(this);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        date = df.format(Calendar.getInstance().getTime());
        if (SharedPreference.getInstance(context).getString("Tech").equalsIgnoreCase(date)) {
            initiaziation();
        } else {
            if (isInternetConneted(context)) {
                tabManager.callGetCategoryApi();
                DBHelperSingleton.getInstance().mDBDbHelper.deleteAllTechFromTable();
            } else {
                initiaziation();
            }
        }
        return rootview;
    }

    private void initiaziation() {
        techArrayLists = new ArrayList<>();
        NewtechArrayLists = new ArrayList<>();
        NewtechArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllTechFromLocalDb();
        if (null != NewtechArrayLists && NewtechArrayLists.size() > 0) {
            NewtechArrayLists = new ArrayList<>();
            NewtechArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllTechFromLocalDb();
            techArrayLists.addAll(NewtechArrayLists);
            setDataAdapter();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessCategory(CategoryResponse categoryResponse) {
//        techArrayLists = new ArrayList<>();
//        techArrayLists.addAll(categoryResponse.getData().getTech());
//        setDataAdapter();
        SharedPreference.getInstance(context).setString("Tech", date);

        for (int i = 0; i < categoryResponse.getData().getTech().size(); i++) {
            TechArrayList contactNumber = new TechArrayList();
            contactNumber.setId(categoryResponse.getData().getTech().get(i).getId());
            contactNumber.setName(categoryResponse.getData().getTech().get(i).getName());
            contactNumber.setCategory(categoryResponse.getData().getTech().get(i).getCategory());
            contactNumber.setUrl(categoryResponse.getData().getTech().get(i).getUrl());
            contactNumber.setImage(categoryResponse.getData().getTech().get(i).getImage());
            DBHelperSingleton.getInstance().mDBDbHelper.insertTech(contactNumber);
        }
        techArrayLists = new ArrayList<>();
        NewtechArrayLists = new ArrayList<>();
        NewtechArrayLists = DBHelperSingleton.getInstance().mDBDbHelper.getAllTechFromLocalDb();
        if (NewtechArrayLists.size() != 0) {
            techArrayLists.addAll(NewtechArrayLists);
            setDataAdapter();
        }
    }

    private void setDataAdapter() {
        TechAdapter newaAdapter = new TechAdapter(context, techArrayLists, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        if (null != gridTech) {
            gridTech.setLayoutManager(layoutManager);
            gridTech.setAdapter(newaAdapter);
        }
    }

    @Override
    public void onError(String errorMessage) {
//        ((TestTab) context).showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
//        ((TestTab) context).showLoader();
    }

    @Override
    public void onHideLoader() {
//        ((TestTab) context).hideLoader();

    }

    @Override
    public void OnitemClick(int position) {

    }
}

