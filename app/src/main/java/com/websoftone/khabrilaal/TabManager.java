package com.websoftone.khabrilaal;

import com.websoftone.khabrilaal.basic.Api;
import com.websoftone.khabrilaal.basic.ApiCallback;
import com.websoftone.khabrilaal.model.CategoryResponse;
import com.websoftone.khabrilaal.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabManager {
    private ApiCallback.CategoryManagerCallback categoryManagerCallback;

    public TabManager(ApiCallback.CategoryManagerCallback categoryManagerCallback) {
        this.categoryManagerCallback = categoryManagerCallback;
    }

    public void callGetCategoryApi(){
        categoryManagerCallback.onShowLoader();
        Api api = ServiceGenerator.createService(Api.class);
        Call<CategoryResponse> planOrderResponseCall = api.callGetCategoryApi();
        planOrderResponseCall.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                categoryManagerCallback.onHideLoader();
                if(response.body().getStatus().equalsIgnoreCase("success")){
                    categoryManagerCallback.onSuccessCategory(response.body());
                }else {
                    categoryManagerCallback.onError("Invalid Token");
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                categoryManagerCallback.onHideLoader();
                if(t instanceof IOException){
                    categoryManagerCallback.onError("Network down or no internet connection");
                }else {
                    categoryManagerCallback.onError("Opps something went wrong!");
                }
            }
        });
    }
}
