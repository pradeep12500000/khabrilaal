
package com.websoftone.khabrilaal.model;

import com.google.gson.annotations.Expose;

public class TechArrayList {

    @Expose
    private String category;
    @Expose
    private String id;
    @Expose
    private String image;
    @Expose
    private String name;
    @Expose
    private String url;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
