
package com.websoftone.khabrilaal.model;

import com.google.gson.annotations.Expose;


public class CategoryResponse {

    @Expose
    private String code;
    @Expose
    private CategoryData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CategoryData getData() {
        return data;
    }

    public void setData(CategoryData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
