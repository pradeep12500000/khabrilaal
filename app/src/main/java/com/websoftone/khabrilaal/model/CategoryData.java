
package com.websoftone.khabrilaal.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class CategoryData {

    @SerializedName("Bollywood")
    private ArrayList<BollywoodArrayList> bollywood;
    @SerializedName("Comedy")
    private ArrayList<ComedyArrayList> comedy;
    @SerializedName("Dance")
    private ArrayList<DanceArrayList> dance;
    @SerializedName("Food")
    private ArrayList<FoodArrayList> food;
    @SerializedName("Money")
    private ArrayList<MoneyArrayList> money;
    @SerializedName("Motovaional")
    private ArrayList<MotovaionalArrayList> motovaional;
    @SerializedName("News")
    private ArrayList<NewsArrayList> news;
    @SerializedName("Tech")
    private ArrayList<TechArrayList> tech;

    public ArrayList<BollywoodArrayList> getBollywood() {
        return bollywood;
    }

    public void setBollywood(ArrayList<BollywoodArrayList> bollywood) {
        this.bollywood = bollywood;
    }

    public ArrayList<ComedyArrayList> getComedy() {
        return comedy;
    }

    public void setComedy(ArrayList<ComedyArrayList> comedy) {
        this.comedy = comedy;
    }

    public ArrayList<DanceArrayList> getDance() {
        return dance;
    }

    public void setDance(ArrayList<DanceArrayList> dance) {
        this.dance = dance;
    }

    public ArrayList<FoodArrayList> getFood() {
        return food;
    }

    public void setFood(ArrayList<FoodArrayList> food) {
        this.food = food;
    }

    public ArrayList<MoneyArrayList> getMoney() {
        return money;
    }

    public void setMoney(ArrayList<MoneyArrayList> money) {
        this.money = money;
    }

    public ArrayList<MotovaionalArrayList> getMotovaional() {
        return motovaional;
    }

    public void setMotovaional(ArrayList<MotovaionalArrayList> motovaional) {
        this.motovaional = motovaional;
    }

    public ArrayList<NewsArrayList> getNews() {
        return news;
    }

    public void setNews(ArrayList<NewsArrayList> news) {
        this.news = news;
    }

    public ArrayList<TechArrayList> getTech() {
        return tech;
    }

    public void setTech(ArrayList<TechArrayList> tech) {
        this.tech = tech;
    }

}
