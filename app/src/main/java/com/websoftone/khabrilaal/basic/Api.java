package com.websoftone.khabrilaal.basic;


import com.websoftone.khabrilaal.model.CategoryResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    @GET("khabri_api.php")
    Call<CategoryResponse> callGetCategoryApi();


}






