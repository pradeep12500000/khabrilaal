package com.websoftone.khabrilaal.basic;


public interface BaseInterface {
    void onError(String errorMessage);
    void onShowLoader();
    void onHideLoader();

}
