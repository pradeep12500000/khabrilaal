package com.websoftone.khabrilaal.basic;


import com.websoftone.khabrilaal.model.CategoryResponse;

public class ApiCallback {
    public interface CategoryManagerCallback extends BaseInterface {
        void onSuccessCategory(CategoryResponse categoryResponse);
    }
}
